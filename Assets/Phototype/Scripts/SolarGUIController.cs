﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SolarGUIController : MonoBehaviour {

	public Text solarPanelText;

	public Text currentValue;
	public Text tensionValue;
	public Text cleaningValue;
	public Text incidenceValue;

	public GameObject currentBar;
	public GameObject tensionBar;
	public GameObject cleaningBar;
	public GameObject incidenceBar;

	public Text inclinationRotationText;

	public SolarPanelController panel;
	public SolarPanel solarPanel;

	public void Start(){
		solarPanelText = gameObject.GetComponentInChildren<Text> ();
	}

	void Update(){
		if(panel != null){
			if(Input.GetButtonDown("Action")){
				panel.triggerAction ();
			}
		}

		if(solarPanel != null){
			if (Input.GetKey (KeyCode.LeftBracket) || Input.GetAxis("Inclination") > 0.5f) {
				if(solarPanel.inclination < 50){
					solarPanel.inclination += 1;
				}
			} else if (Input.GetKey (KeyCode.RightBracket) || Input.GetAxis("Inclination") < -0.5f) {
				if (solarPanel.inclination > 0) {
					solarPanel.inclination -= 1;
				}
			}

			if(Input.GetKey(KeyCode.Period) || Input.GetAxis("Orientation") > 0.5f){
				if (solarPanel.orientation < 359) {
					solarPanel.orientation += 1;
				} else {
					solarPanel.orientation = 0;
				}
			} else if(Input.GetKey(KeyCode.Comma) || Input.GetAxis("Orientation") < -0.5f){
				if (solarPanel.orientation > 0) {
					solarPanel.orientation -= 1;
				} else {
					solarPanel.orientation = 359;
				}
			}
		}

		RefreshPanel (solarPanel);
	}

	public void RefreshPanel(SolarPanelController panel){

		this.panel = panel;

		/*
		solarPanelText.text = "Painel Solar " + panel.idPanel + "\n\n" +
		"Inclinação: " + panel.inclination + "\n" +
		"Rotação: " + panel.orientation + "\n" +
		"Corrente: " + panel.actualA + "a / " + panel.maxA + "a\n" +
		"Tensão: " + panel.actualV + "v / " + panel.maxV + "v\n" +
		"Limpeza: " + panel.cleaning * 100 + "%\n" +
		"Incidencia Solar: " + panel.solarIncidence * 100 + "%";
		*/		

		solarPanelText.text = "Painel Solar " + panel.idPanel;

		currentValue.text = panel.actualA + "a / " + panel.maxA + "a";
		currentBar.transform.localScale = new Vector3 (panel.actualA/panel.maxA, 1, 1);

		tensionValue.text = panel.actualV + "v / " + panel.maxV + "v";
		tensionBar.transform.localScale = new Vector3 (panel.actualV/panel.maxV, 1, 1);

		cleaningValue.text = panel.cleaning * 100 + "%";
		cleaningBar.transform.localScale = new Vector3 (panel.cleaning, 1, 1);

		incidenceValue.text = panel.solarIncidence * 100 + "%";
		incidenceBar.transform.localScale = new Vector3 (panel.solarIncidence, 1, 1);

		inclinationRotationText.text = "Inclinação: " + panel.inclination + " / Rotação: " + panel.orientation;
	}

	public void RefreshPanel(SolarPanel panel){

		this.solarPanel = panel;

		/*
		solarPanelText.text = "Painel Solar " + panel.idPanel + "\n\n" +
		"Inclinação: " + panel.inclination + "\n" +
		"Rotação: " + panel.orientation + "\n" +
		"Corrente: " + panel.actualA + "a / " + panel.maxA + "a\n" +
		"Tensão: " + panel.actualV + "v / " + panel.maxV + "v\n" +
		"Limpeza: " + panel.cleaning * 100 + "%\n" +
		"Incidencia Solar: " + panel.solarIncidence * 100 + "%";
		*/		

		solarPanelText.text = "Painel Solar " + solarPanel.id;

		currentValue.text = panel.current.ToString("F2") + "a / " + panel.maxCurrent + "a";
		currentBar.transform.localScale = new Vector3 (panel.current/panel.maxCurrent, 1, 1);

		tensionValue.text = panel.tension.ToString("F2") + "v / " + panel.maxTension + "v";
		tensionBar.transform.localScale = new Vector3 (panel.tension/panel.maxTension, 1, 1);

		cleaningValue.text = panel.cleaning * 100 + "%";
		cleaningBar.transform.localScale = new Vector3 (panel.cleaning, 1, 1);

		incidenceValue.text = (panel.projectedIncidence * 100).ToString("F2") + "%";
		incidenceBar.transform.localScale = new Vector3 (panel.projectedIncidence, 1, 1);

		inclinationRotationText.text = "Inclinação: " + panel.inclination + " / Rotação: " + panel.orientation;
	}
}
