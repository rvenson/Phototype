﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathBondarie : MonoBehaviour {

	void OnTriggerEnter(Collider other){
		if(other.CompareTag("Player")){
			other.GetComponent<PlayerController> ().Damage (10000);
		}
	}

}
