﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityStandardAssets.Characters.FirstPerson;

public class TeleportUI : MonoBehaviour {

	const float width = 240.0f;
	const float height = 120.0f;

	public Transform area25Object;
	public Transform map;
	public int localSelected = -1;
	public GameObject confirmationMenu;
	public GameObject[] localPanel;
    public GameObject[] localMap;

	public GameObject abandonMenu;
	
	void Start(){

		//Position area25 object
		float area25Lat = height / 90 * GameSettings.instance.area25Lat;
		float area25Lon = width / 180 * GameSettings.instance.area25Lon;
		area25Object.localPosition = new Vector3(area25Lon, area25Lat, 0);

        int stageID = World.instance.stageID;

        for (int i = 1; i < localMap.Length; i++)
        {
            if (i == stageID)
            {
                localMap[i].GetComponent<Image>().color = Color.blue;
                localMap[i].GetComponent<Button>().interactable = false;
            }
            else
            {
                if (GameSettings.instance.missionCompletedList.ContainsKey(i))
                {
                    localMap[i].GetComponent<Image>().color = Color.green;
                    localMap[i].GetComponent<Button>().interactable = false;
                }
            }
        }

        /* 

		//button[World.instance.stageID].SetActive(false);
		button[World.instance.stageID].GetComponent<Button>().enabled = false;
		button[World.instance.stageID].GetComponent<Image>().enabled = false;
		button[World.instance.stageID].GetComponentInChildren<RawImage>().color = Color.red;

		List<GameObject> avaliableButtons = new List<GameObject>();
		foreach(GameObject bt in button){

			if (bt.name == "0") {
				continue;
			}

			bool missionCompleted = false;
			GameManager.gsObject.missionCompletedList.TryGetValue(int.Parse(bt.name), out missionCompleted);

			if(missionCompleted){
				//bt.SetActive (false);
				bt.GetComponent<Button>().enabled = false;
				bt.GetComponent<Image>().enabled = false;
				bt.GetComponentInChildren<RawImage>().color = Color.blue;
			}

		}
		*/
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            gameObject.SetActive(false);
            PauseMenu.instance.TransparentPause(false);
        }
    }

    void OnEnable(){
		PauseMenu.instance.TransparentPause(true);
	}

	void OnDisable(){
		PauseMenu.instance.TransparentPause(false);
	}

	public void SelectLocation(int localID){

		if(localSelected != -1){
			localPanel[localSelected].SetActive(false);
		}
		
		localSelected = localID;
		localPanel[localID].SetActive(true);
	}

	public void ClosePanel(){
		gameObject.SetActive(false);
	}

	public void ConfirmTeleport(){
		if(localSelected != -1){
			confirmationMenu.SetActive(true);
		}
	}

	public void Teleport(){
		if(localSelected != -1){
			confirmationMenu.SetActive(false);
			ClosePanel();
			GameObject.FindGameObjectWithTag("Teleport").GetComponent<TeleportController>().TeleportSequence(localSelected);
		}
	}

	public void CloseConfirmation(){
		confirmationMenu.SetActive(false);
	}

/*

	public void Confirm(){
		Cancel ();

		Debug.Log (MissionSystem2.stageClear);

		//Se ainda não acabou a missão:
		if(MissionSystem2.stageClear){
			ExitTeleport ();
			teleport.TeleportSequence (sceneSelected);
		} else {
			abandonMenu.SetActive(true);
		}
	}

	public void ConfirmAbandon(){
		ExitTeleport ();
		teleport.TeleportSequence (sceneSelected);
	}

	public void CancelAbandon(){
		abandonMenu.SetActive (false);
	}

	public void ExitTeleport(){
		gameObject.SetActive (false);
	}
 */
}
