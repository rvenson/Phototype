﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AimUI : MonoBehaviour {

    public GameObject center;
    public GameObject outside;

    public Color defaultOutsideColor;
    
    private bool isBlinking = false;

    public void FlashAim()
    {
        if (!isBlinking)
        {
            StartCoroutine(BlinkOutside());
        }
    }

    IEnumerator BlinkOutside()
    {
        RawImage img = outside.GetComponent<RawImage>();
        img.color = Color.white;
        yield return new WaitForSeconds(0.15f);
        img.color = defaultOutsideColor;
        isBlinking = false;
    }

}
