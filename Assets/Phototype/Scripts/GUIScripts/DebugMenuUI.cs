﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class DebugMenuUI : MonoBehaviour {

	public Text sceneText;

	public void LoadScene(){
		PauseMenu.instance.TransparentPause ();
		SceneManager.LoadScene (sceneText.text);
		gameObject.SetActive (false);
	}

	public void LoadScene(string name){
		PauseMenu.instance.TransparentPause ();
		SceneManager.LoadScene (name);
		gameObject.SetActive (false);
	}

	public void Close(){
		PauseMenu.instance.TransparentPause ();
		gameObject.SetActive (false);
	}

    public void SaveProgress(int stage)
    {
        SolarPlant plant = GameObject.FindGameObjectWithTag("SolarPlant").GetComponent<SolarPlant>();
        GameSettings gs = GameSettings.instance;
        World world = World.instance;
        gs.missionCompletedList.Add(stage, true);
        float plantPower = (plant.current * plant.tension);
        gs.missionPower.Add(stage, plantPower);
        float finalTime = Time.time - world.startTime;
        gs.missionTimes.Add(stage, finalTime);
    }

}