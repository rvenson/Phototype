﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LanguageUI : MonoBehaviour {

    public Dropdown languageDropdown;
    int index = 0;

    public void Start()
    {
        languageDropdown.onValueChanged.AddListener(delegate {
            index = languageDropdown.value;
            Debug.Log(languageDropdown.value);
        });
    }

    void OnEnable()
    {
        switch(PlayerPrefs.GetString("language", "default"))
        {
            case "default":
                {
                    languageDropdown.value = 0;
                    break;
                }

            case "english":
                {
                    languageDropdown.value = 1;
                    break;
                }
            default:
                {
                    languageDropdown.value = 0;
                    break;
                }
        }
    }

    void OnDisable()
    {
        switch (index)
        {
            case 0:
                {
                    PlayerPrefs.SetString("language", "default");
                    LocalizationManager.instance.LoadLocalization();
                    Debug.Log("saving default");
                    break;
                }
            case 1:
                {
                    PlayerPrefs.SetString("language", "english");
                    LocalizationManager.instance.LoadLocalization();
                    Debug.Log("saving english");
                    break;
                }
            default:
                {
                    break;
                }
        }
    }

}
