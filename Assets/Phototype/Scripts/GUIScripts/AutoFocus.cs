﻿using UnityEngine;
using UnityEngine.EventSystems;

public class AutoFocus : MonoBehaviour {

    public GameObject focus;

    private void OnEnable()
    {
        EventSystem.current.SetSelectedGameObject(focus);
    }

}
