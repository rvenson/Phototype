﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerHUD : MonoBehaviour {

	private PlayerController player;
	public Slider lifeBar;

	void Start(){
		player = GameObject.FindGameObjectWithTag ("Player").GetComponent<PlayerController>();
	}

	void FixedUpdate(){
		lifeBar.value = player.currentHealth;
	}

}
