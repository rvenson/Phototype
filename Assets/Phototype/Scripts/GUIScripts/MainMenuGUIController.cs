﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using GameAnalyticsSDK;

public class MainMenuGUIController : MonoBehaviour {

	public GameObject firstMenu;
    public GameObject languageScreen;
	public GameObject mainMenu;
	public GameObject startMenu;
	public GameObject creditsMenu;
    public GameObject creditsMenu2;
	public GameObject optionsMenu;
	public GameObject loadingScreen;
    public GameObject scoreBoardMenu;

	public GameObject textFirstMenu;

	private bool firstMenuActive = true;
	private Coroutine blinkCoroutine;
	private GameSettings gameSettings;

	void Start(){

        Time.timeScale = 1;

        blinkCoroutine = StartCoroutine (blinkFirstText());
		gameSettings = GameSettings.instance;
        ActiveMouse();

        //PlayerPrefs.DeleteAll();
    }

	void Update(){
		if (firstMenuActive) {
			if (Input.anyKey) {
				StopCoroutine (blinkCoroutine);
				firstMenuActive = false;
				firstMenu.SetActive (false);

                Debug.Log(PlayerPrefs.GetString("language", ""));
                //Testa se o user já selecionou uma lingua
                if (PlayerPrefs.GetString("language", "") == "")
                {
                    LanguageScreen();
                } else
                {
                    MainMenu();
                }
			}
		}
	}

	private IEnumerator blinkFirstText(){
		while (true) {
			textFirstMenu.SetActive (true);
			yield return new WaitForSeconds (1f);
			textFirstMenu.SetActive (false);
			yield return new WaitForSeconds (0.5f);
		}
	}

	public void MainMenu(){
        startMenu.SetActive (false);
		creditsMenu.SetActive (false);
		optionsMenu.SetActive (false);
        languageScreen.SetActive(false);
        creditsMenu2.SetActive(false);
        scoreBoardMenu.SetActive(false);
        mainMenu.SetActive (true);
	}

    public void LanguageScreen()
    {
        startMenu.SetActive(false);
        creditsMenu.SetActive(false);
        optionsMenu.SetActive(false);
        languageScreen.SetActive(true);
        creditsMenu2.SetActive(false);
        scoreBoardMenu.SetActive(false);
        mainMenu.SetActive(false);
    }

	public void StartMenu(){
        if (SceneManager.sceneCountInBuildSettings > 2){
            creditsMenu.SetActive(false);
            optionsMenu.SetActive(false);
            mainMenu.SetActive(false);
            languageScreen.SetActive(false);
            scoreBoardMenu.SetActive(false);
            startMenu.SetActive(true);
        } else{
            SceneManager.LoadScene(1);
        }
		
	}

	public void CreditsMenu(){

        GameAnalytics.NewDesignEvent("MainMenu:Selection:CreditsMenu");

        startMenu.SetActive (false);
		optionsMenu.SetActive (false);
		mainMenu.SetActive (false);
        languageScreen.SetActive(false);
        creditsMenu2.SetActive(false);
        scoreBoardMenu.SetActive(false);
        creditsMenu.SetActive (true);
	}

    public void CreditsMenu2()
    {
        startMenu.SetActive(false);
        optionsMenu.SetActive(false);
        mainMenu.SetActive(false);
        languageScreen.SetActive(false);
        creditsMenu.SetActive(false);
        scoreBoardMenu.SetActive(false);
        creditsMenu2.SetActive(true);
    }

    public void OptionsMenu(){

        GameAnalytics.NewDesignEvent("MainMenu:Selection:OptionsMenu");

        startMenu.SetActive (false);
		creditsMenu.SetActive (false);
		mainMenu.SetActive (false);
        languageScreen.SetActive(false);
        scoreBoardMenu.SetActive(false);
        creditsMenu2.SetActive(false);
        optionsMenu.SetActive (true);
	}

    public void ScoreBoardMenu()
    {
        GameAnalytics.NewDesignEvent("MainMenu:Selection:Score");

        startMenu.SetActive(false);
        optionsMenu.SetActive(false);
        mainMenu.SetActive(false);
        creditsMenu.SetActive(false);
        languageScreen.SetActive(false);
        creditsMenu2.SetActive(false);
        scoreBoardMenu.SetActive(true);
        loadingScreen.SetActive(false);
    }

	public void StartTutorial(){

        GameAnalytics.NewDesignEvent("MainMenu:StartGame:TutorialMode");

        LoadingScreen ();
        SceneManager.LoadScene ("IntroScene");
    }

	public void StartExpertMode(){

        GameAnalytics.NewDesignEvent("MainMenu:StartGame:ExpertMode");

        LoadingScreen ();
		gameSettings.missionCompletedList.Add (-1, true);
		SceneManager.LoadScene ("IntroScene");
	}

	public void LoadingScreen(){
        startMenu.SetActive (false);
		optionsMenu.SetActive (false);
		mainMenu.SetActive (false);
		creditsMenu.SetActive (false);
        languageScreen.SetActive(false);
        creditsMenu2.SetActive(false);
        scoreBoardMenu.SetActive(false);
        loadingScreen.SetActive (true);
	}

	public void ExitMenu(){
		Application.Quit ();
	}

    public void ActiveMouse()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

}
