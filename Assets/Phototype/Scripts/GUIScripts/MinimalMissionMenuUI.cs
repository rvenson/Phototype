﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MinimalMissionMenuUI : MonoBehaviour {

	public MissionSystem2 missions;
	public GameObject ui;
	public Transform viewport;

	void Start(){
		StartCoroutine(StartMenu ());
	}

	IEnumerator StartMenu(){

		yield return new WaitForSeconds (2f);

		while (true) {

			Refresh ();

			int yPos = 70;

			foreach (Transform task in missions.gameObject.transform) {
				Objective objective = task.GetComponent<Objective> ();

				if (objective.active && objective.visible && !objective.sucess) {
					GameObject obj = Instantiate (ui, viewport);
					RectTransform trans = obj.GetComponent<RectTransform> ();
					Vector2 pos = new Vector2 (0, yPos);
					trans.anchoredPosition = pos;



					obj.transform.Find ("Title").GetComponent<Text> ().text = LocalizationManager.instance.GetLocalizedValue(objective.objectiveName);
					obj.transform.Find ("Desc").GetComponent<Text> ().text = LocalizationManager.instance.GetLocalizedValue(objective.shortDesc);
					//obj.transform.Find ("State").GetComponent<Text>().text = objective.GetSucessString();

					yPos -= 42;
				}
			}

			yield return new WaitForSeconds (2f);

		}
	}

	void Refresh(){
		foreach (Transform child in viewport.transform) {
			GameObject.Destroy(child.gameObject);
		}
	}
}
