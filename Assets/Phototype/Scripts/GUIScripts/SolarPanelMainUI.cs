﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SolarPanelMainUI : MonoBehaviour {

	public Text solarPanelText;

	public Text currentValue;
	public Text tensionValue;
	public Text cleaningValue;
	public Text incidenceValue;

	public Slider currentBar;
	public Slider tensionBar;
	public Slider cleaningBar;
	public Slider incidenceBar;

	public Slider inclinacaoSlider;
	public Slider rotacaoSlider;
	public Text inclinacaoText;
	public Text rotacaoText;

	public SolarPanel solarPanel;
	private int originalInclination;
	private int originalRotation;

	void Start(){
		solarPanelText = gameObject.GetComponentInChildren<Text> ();
		Listeners ();
	}

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            gameObject.SetActive(false);
            PauseMenu.instance.TransparentPause(false);
        }
    }

    void OnEnable(){
		PauseMenu.instance.TransparentPause ();
		originalInclination = (int) solarPanel.inclination;
		originalRotation = (int) solarPanel.orientation;
	}

	void OnDisable(){
		PauseMenu.instance.TransparentPause ();
	}

	public void RefreshPanel(SolarPanel panel){

		this.solarPanel = panel;

        //solarPanelText.text = "Painel Solar " + solarPanel.id;

        currentValue.text = panel.current.ToString("F2") + LocalizationManager.instance.GetLocalizedValue("label.currentShort") + " / " + panel.maxCurrent + LocalizationManager.instance.GetLocalizedValue("label.currentShort");
		currentBar.value = panel.current/panel.maxCurrent;

		tensionValue.text = panel.tension.ToString("F2") + LocalizationManager.instance.GetLocalizedValue("label.tensionShort") + " / " + panel.maxTension + LocalizationManager.instance.GetLocalizedValue("label.tensionShort");
		tensionBar.value = panel.tension/panel.maxTension;

		cleaningValue.text = panel.cleaning * 100 + "%";
		cleaningBar.value = panel.cleaning;

		incidenceValue.text = (panel.projectedIncidence * 100).ToString("F2") + "%";
		incidenceBar.value = panel.projectedIncidence;

		inclinacaoSlider.value = panel.inclination;
		inclinacaoText.text = panel.inclination.ToString("0 'º'");

		if(panel.orientation < 90){
			rotacaoSlider.value = 0;
			rotacaoText.text = panel.orientation.ToString(LocalizationManager.instance.GetLocalizedValue("label.north"));
		} else if(panel.orientation < 180){
			rotacaoSlider.value = 1;
			rotacaoText.text = panel.orientation.ToString(LocalizationManager.instance.GetLocalizedValue("label.east"));
		} else if(panel.orientation < 270){
			rotacaoSlider.value = 2;
			rotacaoText.text = panel.orientation.ToString(LocalizationManager.instance.GetLocalizedValue("label.south"));
		} else if(panel.orientation < 360){
			rotacaoSlider.value = 3;
			rotacaoText.text = panel.orientation.ToString(LocalizationManager.instance.GetLocalizedValue("label.west"));
		}
	}

	private void Listeners(){
		rotacaoSlider.onValueChanged.AddListener(
			delegate { 

				if(rotacaoSlider.value == 0){
					solarPanel.orientation = (int) 0;
					rotacaoText.text = solarPanel.orientation.ToString(LocalizationManager.instance.GetLocalizedValue("label.north"));
				} else if(rotacaoSlider.value == 1){
					solarPanel.orientation = (int) 90;
					rotacaoText.text = solarPanel.orientation.ToString(LocalizationManager.instance.GetLocalizedValue("label.east"));
				} else if(rotacaoSlider.value == 2){
					solarPanel.orientation = (int) 180;
					rotacaoText.text = solarPanel.orientation.ToString(LocalizationManager.instance.GetLocalizedValue("label.south"));
				} else if(rotacaoSlider.value == 3){
					solarPanel.orientation = (int) 270;
					rotacaoText.text = solarPanel.orientation.ToString(LocalizationManager.instance.GetLocalizedValue("label.west"));
				}
			}
		);

		inclinacaoSlider.onValueChanged.AddListener(
			delegate { 
				solarPanel.inclination = (int) inclinacaoSlider.value;
				inclinacaoText.text = solarPanel.inclination.ToString("0 'º'");
			}
		);
	}

	public void ConfirmPanelMenu(){
		gameObject.SetActive (false);
		solarPanel.StartRotor();
	}

	public void CancelPanelMenu(){
		solarPanel.inclination = originalInclination;
		solarPanel.orientation = originalRotation;
		solarPanel = null;
		gameObject.SetActive (false);
	}
}
