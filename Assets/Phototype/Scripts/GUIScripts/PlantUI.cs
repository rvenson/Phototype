﻿using UnityEngine;
using UnityEngine.UI;

public class PlantUI : MonoBehaviour {

	public SolarGrid selectedGrid;

    public GameObject gridObject;
	public Text gridTextTension;
	public Text gridTextCurrent;
	public Slider gridSliderTension;
	public Slider gridSliderCurrent;
    public Text selectedGridName;

	public Text gridAStatus;
	public Text gridBStatus;
	public Text gridCStatus;

	public Slider plantSliderTension;
	public Slider plantSliderCurrent;
    public Text plantTotalTension;
    public Text plantTotalCurrent;

    private SolarPlant plant;
    SolarGrid grid1;
    SolarGrid grid2;
    SolarGrid grid3;

    bool activePanel = false;

    void Start(){
		int stageID = World.instance.stageID;

		if (stageID >= 1 && stageID <= 3) {
			gameObject.SetActive (true);

            plant = GameObject.FindGameObjectWithTag("SolarPlant").GetComponent<SolarPlant>();
            grid1 = plant.gridList.GetChild(0).GetComponent<SolarGrid>();
            grid2 = plant.gridList.GetChild(1).GetComponent<SolarGrid>();
            grid3 = plant.gridList.GetChild(2).GetComponent<SolarGrid>();

            activePanel = true;

        } else {
			gameObject.SetActive (false);
		}
    }

    void FixedUpdate()
    {
        if (activePanel)
        {
            plantSliderCurrent.value = plant.current;
            plantSliderTension.value = plant.tension;
            plantTotalCurrent.text = plant.current.ToString("F1") + LocalizationManager.instance.GetLocalizedValue("label.currentShort");
            plantTotalTension.text = plant.tension.ToString("F1") + LocalizationManager.instance.GetLocalizedValue("label.tensionShort");

            if (grid1.isOn)
            {
                gridAStatus.text = LocalizationManager.instance.GetLocalizedValue("label.online");
            }
            else
            {
                gridAStatus.text = LocalizationManager.instance.GetLocalizedValue("label.offline");
            }

            if (grid1.GetPower() < 3000.0f)
            {
                gridAStatus.text += "\n(" + LocalizationManager.instance.GetLocalizedValue("label.lowPower") + "!)";
            }

            if (grid2.isOn)
            {
                gridBStatus.text = LocalizationManager.instance.GetLocalizedValue("label.online");
            }
            else
            {
                gridBStatus.text = LocalizationManager.instance.GetLocalizedValue("label.offline");
            }

            if (grid2.GetPower() < 3000.0f)
            {
                gridBStatus.text += "\n(" + LocalizationManager.instance.GetLocalizedValue("label.lowPower") + "!)";
            }


            if (grid3.isOn)
            {
                gridCStatus.text = LocalizationManager.instance.GetLocalizedValue("label.online");
            }
            else
            {
                gridCStatus.text = LocalizationManager.instance.GetLocalizedValue("label.offline");
            }

            if (grid3.GetPower() < 3000.0f)
            {
                gridCStatus.text += "\n(" + LocalizationManager.instance.GetLocalizedValue("label.lowPower") + "!)";
            }

            if (selectedGrid != null)
            {
                gridTextTension.text = selectedGrid.tension.ToString("F1");
                gridTextCurrent.text = selectedGrid.current.ToString("F1");
                gridSliderTension.value = selectedGrid.tension;
                gridSliderCurrent.value = selectedGrid.current;
                selectedGridName.text = selectedGrid.name;
            }
        }
    }

    public void SetSelectedGrid(SolarGrid sg)
    {

        selectedGrid = sg;

        if (sg == null)
        {
            gridObject.SetActive(false);
        }
        else
        {
            gridObject.SetActive(true);
        }
    }

}
