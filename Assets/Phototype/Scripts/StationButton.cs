﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StationButton : InteractableObject {

	public GameObject button;
	public GameObject stationLight;
	public SerialPanelController stateUI;
	public int station = 0;
	public float timeReady = 20.0f;
	public HordeSpawn hordeController;
	private bool active = false;
	public bool isOnline = false;
	
	public override void triggerAction(){
		if(!active){
			StartCoroutine (ActiveStation ());
			active = true;
		}
	}

	private IEnumerator ActiveStation(){
		stationLight.GetComponent<StationLight> ().StandByStation();
		button.GetComponent<Renderer> ().material.color = Color.yellow;
		stateUI.StandBy (station);

		hordeController.WakeUpHorde ();

		yield return new WaitForSeconds (timeReady);

		stationLight.GetComponent<StationLight> ().ActiveStation();
		button.GetComponent<Renderer> ().material.color = Color.green;
		stateUI.Active (station);
		active = false;
		isOnline = true;
	}

	public void ActiveStationByPass(){
		stationLight.GetComponent<StationLight> ().ActiveStation();
		button.GetComponent<Renderer> ().material.color = Color.green;
		stateUI.Active (station);
		isOnline = true;
	}
}
