﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuizItem : InteractableObject {

	public Question question;
	public GameObject explosion;

	public override string TipText ()
	{
		return LocalizationManager.instance.GetLocalizedValue("text.tip.quiz"); ;
	}

	public override void triggerAction ()
	{
		Quiz.instance.NewQuestion(question, this);
	}

	public void ItemEffect(){
		PlayerController player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
		player.AddLife(50);

		AudioManager.instance.PlayEffect("efx_cure");

		Destroy(gameObject);
	}

	public void ItemBadEffect(){
		//PlayerController player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
		Instantiate(explosion, gameObject.transform.position, gameObject.transform.rotation);
		//player.Damage(50);


		Destroy(gameObject);
	}
	
}
