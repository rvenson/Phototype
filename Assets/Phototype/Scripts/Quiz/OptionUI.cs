﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionUI : MonoBehaviour {

	public Text optionText;
	public QuizUI quizUI;

	public void Answer(){
		Debug.Log(optionText.text);
		quizUI.CheckAnswer(optionText.text);
	}
	
}
