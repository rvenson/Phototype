﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage02Controller : MonoBehaviour {

	public int missionID;
	private GameSettings gameSettings;

	public MessagesController message;

	void Start(){

		AudioManager.instance.PlayMusic ("music_titan");

		bool value = false;
		if(GameObject.FindGameObjectWithTag ("GameSettings") != null){
			gameSettings = GameObject.FindGameObjectWithTag ("GameSettings").GetComponent<GameSettings> ();
			gameSettings.missionCompletedList.TryGetValue (missionID, out value);
		}

		//Se o cenário não ficou completo
		if(!value){
			StartCoroutine (InitialSequence());
		} 
		//Se o cenário já está completo
		else {
			
		}

        string initialMessage = LocalizationManager.instance.GetLocalizedValue("mission.stage02.entry");

        GameObject.FindGameObjectWithTag ("Canvas")
			.GetComponent<MessagesController>().messageUI.ShowMessage(initialMessage);
	}

	private IEnumerator InitialSequence(){
		List<string> dialog = new List<string> ();

        dialog.Add(LocalizationManager.instance.GetLocalizedValue("mission.stage02.dialog01"));
        dialog.Add(LocalizationManager.instance.GetLocalizedValue("mission.stage02.dialog02"));
        dialog.Add(LocalizationManager.instance.GetLocalizedValue("mission.stage02.dialog03"));
        yield return new WaitForSeconds (5f);
		message.SendDialog (dialog);

        yield return new WaitForSeconds(30f);
        dialog = new List<string>();
        dialog.Add(LocalizationManager.instance.GetLocalizedValue("mission.stage02.dialog04"));
        dialog.Add(LocalizationManager.instance.GetLocalizedValue("mission.stage02.dialog05"));
        message.SendDialog(dialog);
    }
}
