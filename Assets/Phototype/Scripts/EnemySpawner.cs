﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour {

	public GameObject enemy;

	void OnTriggerEnter(Collider other){
		if(Random.value < 0.7){
			if (other.CompareTag ("Player")) {

				Vector3 spawnPosition = gameObject.transform.position +
					new Vector3 (0, 5, 0);

				Instantiate (enemy, spawnPosition, new Quaternion (), gameObject.transform);
			}
		}
	}
}
