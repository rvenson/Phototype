﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IGoal{

	bool GetCompleted();
	string GetName();
	string GetDesc();
	string GetLongDesc();
	IEnumerator StartGoal(System.Action<bool> done);
	void FinalizeGoal();
}
