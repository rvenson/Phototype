﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;
using UnityEngine.SceneManagement;
using GameAnalyticsSDK;

public class TeleportController: MonoBehaviour {

	public GameObject teleportGUIPanel;
	public GameObject teleportBackGUIPanel;
	public GameObject eletricalSparks;
	public GameObject screenOverlayImage;
	public TeleportActivator activator01;
	public TeleportActivator activator02;

	private GameObject menu;
	private Animator anim;
	private AudioSource audioSrc;
	private GameSettings gameSettings;
	private GameObject player;

	public bool isUsed = false;
	public bool isStage = true;

	private float startTime;

	void Start(){
		gameSettings = GameSettings.instance;
		anim = gameObject.GetComponentInParent<Animator> ();
		audioSrc = gameObject.GetComponent<AudioSource> ();
		AudioManager.instance.RegisterSpatialSource(audioSrc);
		player = GameObject.FindGameObjectWithTag ("Player");
	}

	void OnTriggerEnter(Collider other){
		if(Time.time - startTime > 5){
			OpenMenu ();
		}
	}


	void OnEnable(){
		startTime = Time.time;
	}

	void Update(){

		if(isUsed){
			player.transform.position = gameObject.transform.position + new Vector3(0, 0.8f, 0);
		}
	}

	void OnTriggerStay(){
		if(Input.GetKeyDown(KeyCode.Escape)){
			ExitMenu ();
		}

		if(Input.GetKeyDown(KeyCode.Alpha1)){
			if (isStage) {
				TeleportSequence (0);
			} else {
				TeleportSequence (1);
			}
		}

		if(Input.GetKeyDown(KeyCode.Alpha2)){
			if (isStage) {
				TeleportSequence (0);
			} else {
				TeleportSequence (2);
			}
		}

		if(Input.GetKeyDown(KeyCode.Alpha3)){
			if (isStage) {
				TeleportSequence (0);
			} else {
				TeleportSequence (3);
			}
		}
	}

	void OnTriggerExit(){
		ExitMenu ();
	}

	private void ExitMenu(){
		Destroy(menu);
	}

	private void OpenMenu(){
		if (isStage) {
			menu = Instantiate (teleportBackGUIPanel, GameObject.FindGameObjectWithTag ("Canvas").transform);
			menu.transform.localPosition = new Vector3 (0, 0, 0);
		} else {
			menu = Instantiate (teleportGUIPanel, GameObject.FindGameObjectWithTag ("Canvas").transform);
			menu.transform.localPosition = new Vector3 (0, 0, 0);
		}
	}

	public void TeleportSequence(int scene){
		isUsed = true;
		activator01.isActivated = false;
		activator02.isActivated = false;

		//close menu
		//ExitMenu ();

		//deactivate player control
		//StartCoroutine (LerpToFront(player));
		//RigidbodyFirstPersonController controller = player.GetComponent<RigidbodyFirstPersonController> ();
		//controller.enabled = false;

		//start animation
		anim.SetBool("isTeleporting", true);
		audioSrc.PlayDelayed (2.5f);
		Animator playerAnim = player.GetComponent<Animator> ();
		playerAnim.SetBool ("isTeleporting", true);
		StartCoroutine (LoadingStage(scene));
	}

	private IEnumerator LerpToFront(GameObject player){

		float time = 3;
		float elapsedTime = 0;
		Vector3 startPosition = player.transform.localEulerAngles;
		Debug.Log (startPosition);
		Vector3 endPosition = new Vector3 (0, 270, 0);
		Debug.Log (endPosition);
		Vector3 position;

		while(elapsedTime < time){
			position = Vector3.Lerp (startPosition, endPosition, (elapsedTime / time));
			player.transform.eulerAngles = position;
			elapsedTime += Time.deltaTime;
			yield return new WaitForEndOfFrame ();
		}
	}

	private IEnumerator LoadingStage(int scene){
		yield return new WaitForSeconds (4);
		eletricalSparks.SetActive (true);
		yield return new WaitForSeconds (3);
		Instantiate (screenOverlayImage, GameObject.FindGameObjectWithTag ("Canvas").transform);
		yield return new WaitForSeconds (3f);

		bool finalReady = false;
		gameSettings.missionCompletedList.TryGetValue (1, out finalReady);
		Debug.Log ("Verificando estagio 01" + finalReady);
		if(finalReady){
			finalReady = false;
			gameSettings.missionCompletedList.TryGetValue (2, out finalReady);
			Debug.Log ("Verificando estagio 02" + finalReady);
			if(finalReady){
				finalReady = false;
				gameSettings.missionCompletedList.TryGetValue (3, out finalReady);
				Debug.Log ("Verificando estagio 03" + finalReady);
				if(finalReady){
					Debug.Log ("Carregando estagio final");
					SceneManager.LoadScene ("FinalStage");
				}
			}
		}

		if(!finalReady){
			switch (scene) {
			case 0:
				{
                        GameAnalytics.NewDesignEvent("Teleport:"+ SceneManager.GetActiveScene().name +":ControlRoom");
                        SceneManager.LoadScene ("ControlRoom");
					break;
				}
			case 1:
				{
                        GameAnalytics.NewDesignEvent("Teleport:" + SceneManager.GetActiveScene().name + ":Stage01");
                        SceneManager.LoadScene ("Stage01");
					break;
				}
			case 2:
				{
                        GameAnalytics.NewDesignEvent("Teleport:" + SceneManager.GetActiveScene().name + ":Stage02");
                        SceneManager.LoadScene ("Stage02");
					break;
				}
			case 3:
				{
                        GameAnalytics.NewDesignEvent("Teleport:" + SceneManager.GetActiveScene().name + ":Stage03");
                        SceneManager.LoadScene ("Stage03");
					break;
				}
			default:
				{
					break;
				}
			}
		}
	}

	/*

	private void LockCursor(bool option){
		if (option) {
			GameObject player = GameObject.FindGameObjectWithTag ("Player");
			RigidbodyFirstPersonController controller = player.GetComponent<RigidbodyFirstPersonController> ();
			controller.mouseLook.lockCursor = true;
			controller.enabled = true;
		} else {
			GameObject player = GameObject.FindGameObjectWithTag ("Player");
			RigidbodyFirstPersonController controller = player.GetComponent<RigidbodyFirstPersonController> ();
			controller.mouseLook.lockCursor = false;
			Cursor.lockState = CursorLockMode.None;
			controller.enabled = false;
		}
	}
	*/
}
