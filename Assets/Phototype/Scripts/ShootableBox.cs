﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShootableBox : Destroyable {

	public int currentHealth = 30;
	public bool changeColor = true;
	public GameObject deathAnimation;
	public TextMesh lifeText;
	public AudioSource deathSound;

	private Color originalColor;
	private Renderer rend;

	private int maxHealth;

	public void Start(){
		maxHealth = currentHealth;
		UpdateHealth ();
		AudioManager.instance.RegisterSpatialSource(deathSound);
	}

	public override void Damage(int damage){
		currentHealth -= damage;
		UpdateHealth ();
		if (currentHealth <= 0) {
			if(deathAnimation != null){
				deathAnimation.SetActive (true);
				PlayDeathSound ();
			}
			StartCoroutine (Unspawn());
		} else {
			//setcolor
			if (changeColor) {
				StartCoroutine (DamageColor ());
			}
		}
	}

	private IEnumerator DamageColor(){
		rend = GetComponent<Renderer> ();
		Color originalColor = rend.material.color;
		rend.material.color = Color.red;
		yield return new WaitForSeconds (0.3f);
		rend.material.color = originalColor;

	}

	private IEnumerator Unspawn (){
		if(changeColor){
			rend.material.color = Color.clear;
		}
		yield return new WaitForSeconds (3);
		Destroy (gameObject);
	}

	private void UpdateHealth(){
		lifeText.text = "Vida: " + currentHealth + "/" + maxHealth;
	}

	private void PlayDeathSound(){
		if(deathSound != null){
			deathSound.Play ();
		}
	}
}
