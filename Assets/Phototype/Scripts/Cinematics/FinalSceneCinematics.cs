﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class FinalSceneCinematics : MonoBehaviour {

	public MessageUI mainText;
	public GameObject continueText;
	public GameObject loadingScreen;
	public GameObject main01;
	public GameObject animationObject;
	public GameObject initBackground;

	private bool continuePressed = false;
	private bool continueEnabled = false;

	void Start(){

		StartCoroutine (ContinueTextToggle());

		List<string> msg = new List<string> ();
		msg.Add (LocalizationManager.instance.GetLocalizedValue("mission.end.dialog01"));
		msg.Add (LocalizationManager.instance.GetLocalizedValue("mission.end.dialog02"));
        msg.Add (LocalizationManager.instance.GetLocalizedValue("mission.end.dialog03"));
        msg.Add (LocalizationManager.instance.GetLocalizedValue("mission.end.dialog04"));
        msg.Add (LocalizationManager.instance.GetLocalizedValue("mission.end.dialog05"));
        msg.Add(" ");
        mainText.setPool (msg);
		mainText.gameObject.SetActive (true);


	}

	void Update(){
		if(continueEnabled){
			if(Input.GetKeyDown(KeyCode.Escape)){
				continuePressed = true;
			}
		}

        if (!mainText.gameObject.activeSelf)
        {
            continuePressed = true;
        }

        if (continuePressed){
			main01.SetActive (false);
			initBackground.SetActive (true);
			animationObject.SetActive (true);
			//LoadingScreen ();
			//SceneManager.LoadScene ("ControlRoom");
		}
	}

	private IEnumerator ContinueTextToggle(){
		yield return new WaitForSeconds (3f);
		continueText.SetActive (true);
		continueEnabled = true;
	}

	private void LoadingScreen(){
		loadingScreen.SetActive (true);
	}
}
