﻿using UnityEngine;
using System.Collections;

public class InteractableObject : MonoBehaviour
{

	public bool isActivated = true;

	public virtual void triggerAction(){
		Debug.Log ("Action triggered on " + gameObject.name);
	}

	public virtual void OnMouseEvent(){
		//
	}

	public virtual void OnMouseOverEvent(){
		//
	}

	public virtual string TipText(){
		//return "Interaja com esse objeto pressionando a tecla E";
        return LocalizationManager.instance.GetLocalizedValue("text.tip.objectInteraction");
    }
}

