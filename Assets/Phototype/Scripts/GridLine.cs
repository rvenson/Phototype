﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridLine : MonoBehaviour {

	public GameObject gridLine;
	public GameObject solarTo;
	public GameObject solarFrom;

	private LineRenderer line;

	void Start(){
		line = gridLine.GetComponent<LineRenderer> ();

		line.SetPosition (0, solarTo.transform.position + new Vector3(0, 1, 0));
		line.SetPosition (1, solarFrom.transform.position + new Vector3(0, 1, 0));
	}

}
