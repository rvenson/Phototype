﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallDamage : MonoBehaviour {

	private Rigidbody rg;
	private PlayerController player;
	private float lastVelocity = 0f;

	void Start(){
		rg = gameObject.GetComponent<Rigidbody> ();
		player = gameObject.GetComponent<PlayerController> ();
	}

	void FixedUpdate(){

		if(rg.velocity.y - lastVelocity > 10){
			int damage = (int) (rg.velocity.y - lastVelocity);
			player.Damage (damage * 2);
		}

		lastVelocity = rg.velocity.y;
	}
}
