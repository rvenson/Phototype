﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class LocalizationManager : MonoBehaviour {

    public static LocalizationManager instance;
    private Dictionary<string, string> localizedText;
    private QuizData quizData;
    private string missingTextString = "Localized text not found";

    private bool isLocalizedTextLoaded = false;
    private bool isQuizLoaded = false;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;

            LoadLocalization();

        } else
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }

    public void LoadLocalization()
    {
        StartCoroutine(LoadFile("language_" + GetLanguage() + ".json", LoadLocalizedData));
        StartCoroutine(LoadFile("quiz_" + GetLanguage() + ".json", LoadQuizData));
    }

    IEnumerator LoadFile(string fileName, System.Action<string> callback)
    {
        string filePath = Path.Combine(Application.streamingAssetsPath, fileName);
        Debug.Log("Loading file path " + filePath);

        if (filePath.Contains("://") || filePath.Contains(":///"))
        {
            Debug.Log(filePath);
            WWW www = new WWW(filePath);
            yield return www;

            Debug.Log(" Page Size : " + www.size);
            Debug.Log(" Page Size : " + www.error);

            if (www.error != "")
            {
                callback(www.text);
            }
            else
            {
                callback(null);
            }
        }
        else
        {
            if (File.Exists(filePath))
            {
                callback(File.ReadAllText(filePath));
            } else
            {
                callback(null);
            }
        }

        yield return null;
    }

    private void LoadLocalizedData(string result)
    {
        localizedText = new Dictionary<string, string>();

        if (result != null)
        {
            LocalizationData loadedData = JsonUtility.FromJson<LocalizationData>(result);

            for (int i = 0; i < loadedData.items.Length; i++)
            {
                localizedText.Add(loadedData.items[i].key, loadedData.items[i].value);
            }

            Debug.Log("Loaded localization data (" + GetLanguage() + ") with " + localizedText.Count + " entries");
        }
        else
        {
            Debug.LogError("Cannot find localized text (" + GetLanguage() + ")");
        }
    }

    private void LoadQuizData(string result)
    {

        if (result != null)
        {
            LocalizationData loadedData = JsonUtility.FromJson<LocalizationData>(result);
            quizData = JsonUtility.FromJson<QuizData>(result);
            Debug.Log("Loaded quiz database (" + GetLanguage() + ") with " + quizData.questions.Length + " questions");
        }
        else
        {
            Debug.LogError("Cannot find quiz database (" + GetLanguage() + ")");
        }
    }

    public string GetLanguage()
    {
        return PlayerPrefs.GetString("language", "default");
    }

    public string GetLocalizedValue(string key)
    {
        if(localizedText.ContainsKey(key)){
            string result = missingTextString;
            localizedText.TryGetValue(key, out result);
            return result;
        }

        Debug.LogError("String key " + key + " not found!");
        return missingTextString;

    }

    public Question[] GetQuestions()
    {
        return quizData.questions;
    }

    public bool GetIsReady()
    {
        if (isLocalizedTextLoaded && isQuizLoaded)
        {
            return true;
        } else
        {
            return false;
        }
    }

}
