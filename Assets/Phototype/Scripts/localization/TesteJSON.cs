﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class TesteJSON : MonoBehaviour {

    public Question[] teste;
    List<Question> questions = new List<Question>();

    private void Start()
    {

        string playerToJason = "";

        foreach (Question q in teste) {
            playerToJason += JsonUtility.ToJson(q, true) + ",\n";
        }

        LoadLocalizedText("quiz_default.json");
    }


    public void LoadLocalizedText(string fileName)
    {
        string filePath = Path.Combine(Application.streamingAssetsPath, fileName);

        if (File.Exists(filePath))
        {
            string dataAsJson = File.ReadAllText(filePath);
            QuizData loadedData = JsonUtility.FromJson<QuizData>(dataAsJson);

            Debug.Log(loadedData.questions.Length);

            //Debug.Log("Loaded localization data with " + localizedText.Count + " entries");

        }
    }

}