﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameAnalyticsSDK;

public class ObjectiveStage01M01 : Objective {

	public SolarGrid grid;

	public override IEnumerator ObjectiveTrigger(){

        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, "Stage0" + World.instance.stageID, gameObject.name);

        while (!sucess){

			if ((grid.tension * grid.current) > 3000.0f) {
				sucess = true;
				OnObjectiveSucess ();
				Debug.Log ("Finalizado");

                GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, "Stage0" + World.instance.stageID, gameObject.name);
                break;
			}

			yield return new WaitForSeconds (1.0f);
		}

		yield return null;
	}
}
