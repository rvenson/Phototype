﻿using System.Collections;
using UnityEngine;

public class Objective : MonoBehaviour {

	public string objectiveName = "teste";
	public string shortDesc = "";
	public string longDesc = "";
	public bool sucess = false;
	public bool active = true;
	public bool visible = true;

	public Objective nextObjective;

	public virtual IEnumerator ObjectiveTrigger(){
		yield return null;
	}

	public virtual string GetSucessString(){
		if (sucess) {
			return LocalizationManager.instance.GetLocalizedValue("label.completed");
		} else {
			return LocalizationManager.instance.GetLocalizedValue("label.active");
        }
	}

	public virtual void OnObjectiveSucess(){

		AudioManager.instance.PlayEffect ("efx_sucess");

		if(nextObjective != null){
			nextObjective.active = true;
			StartCoroutine(nextObjective.ObjectiveTrigger ());
		}
	}
}
