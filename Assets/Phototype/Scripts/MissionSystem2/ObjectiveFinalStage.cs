﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameAnalyticsSDK;

public class ObjectiveFinalStage : Objective {

	public BossController boss;

	public override IEnumerator ObjectiveTrigger(){

        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, "Stage0" + World.instance.stageID, gameObject.name);

        while (!sucess){

			if(!boss.isAlive){

                GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, "Stage0" + World.instance.stageID, gameObject.name);
                sucess = true;
				Debug.Log ("Finalizado todos");
				OnObjectiveSucess ();
				SaveProgress ();

				break;
			}

			yield return new WaitForSeconds (1.0f);
		}

		yield return null;
	}

	private void SaveProgress(){
		SolarPlant plant = GameObject.FindGameObjectWithTag ("SolarPlant").GetComponent<SolarPlant> ();
		GameSettings gs = GameSettings.instance;
		World world = GameObject.FindGameObjectWithTag ("World").GetComponent<World> ();
		gs.missionCompletedList.Add (world.stageID, true);
		float plantPower = (plant.current * plant.tension);
		gs.missionPower.Add(world.stageID, plantPower);
		float finalTime = Time.time - world.startTime;
		gs.missionTimes.Add (world.stageID, finalTime);
	}
}
