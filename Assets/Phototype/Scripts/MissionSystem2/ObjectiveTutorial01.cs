﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameAnalyticsSDK;

public class ObjectiveTutorial01 : Objective {

	public GameObject gun;

	public override IEnumerator ObjectiveTrigger(){

        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, "Tutorial", "Objective01");

        Debug.Log ("Pickup the gun!");
		ShowMessages ();

		while(!sucess){

			if (gun.activeSelf) {
				sucess = true;
				OnObjectiveSucess ();
				Debug.Log ("Finalizado");
                GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, "Tutorial", "Objective01");
                break;
			}

			yield return new WaitForSeconds (1.0f);
		}

		yield return null;
	}

	public void ShowMessages(){
		MessagesController messages = GameObject.FindGameObjectWithTag ("Canvas").GetComponent<MessagesController> ();

		List<string> dialog = new List<string> ();
		dialog.Add (LocalizationManager.instance.GetLocalizedValue("mission.contromRoom.objective01.dialog01"));
        dialog.Add(LocalizationManager.instance.GetLocalizedValue("mission.contromRoom.objective01.dialog02"));
        dialog.Add(LocalizationManager.instance.GetLocalizedValue("mission.contromRoom.objective01.dialog03"));
        messages.SendDialog (dialog);
	}

}
