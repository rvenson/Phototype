﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorHandler : MonoBehaviour {

	public bool active = true;
	private Animator anim;
	private AudioSource audioSrc;

	private bool open = false;

	void Start () {
		anim = gameObject.GetComponentInChildren<Animator> ();
		audioSrc = gameObject.GetComponent<AudioSource> ();

		AudioManager.instance.RegisterSpatialSource(audioSrc);
	}

	void OnTriggerEnter(){
		if(active && !open){
			anim.SetBool ("isOpen", true);
			open = true;
			audioSrc.Play ();
		} else
        {
            if (!active && !open)
            {
                CanvasController.instance.tipLayer.GetComponent<TipLayer>().ShowText(LocalizationManager.instance.GetLocalizedValue("text.tip.door.disabled"), 3f, true);
            }
        }
	}

	void OnTriggerExit(){
		if (active && open) {
			anim.SetBool ("isOpen", false);
			open = false;
			audioSrc.Play ();
		}
	}

	public void OpenDoor(){
		anim.SetBool ("isOpen", true);
		open = true;
		audioSrc.Play ();
	}

	public void BlockDoor(){
		anim.SetBool ("isOpen", false);
		open = false;
	}
}
