﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterTrigger : MonoBehaviour {

	public GameObject multigun;
	public PlayerFoot foot;

	public AudioClip waterSplash;
	public AudioClip waterExit;
	public AudioClip waterStay;

	void OnTriggerEnter(Collider other){
		if(other.CompareTag("Player")){
			multigun.SetActive (false);
			foot.foot.clip = waterSplash;
			foot.foot.Play ();
		}
	}

	void OnTriggerExit(Collider other){
		if(other.CompareTag("Player")){
			multigun.SetActive (true);
			foot.foot.clip = waterExit;
			foot.foot.Play ();
		}
	}

	void OnTriggerStay(Collider other){
		if(other.CompareTag("Player")){
			if(!foot.foot.isPlaying){
				foot.foot.clip = waterStay;
				foot.foot.Play ();
			}
		}
	}
}
