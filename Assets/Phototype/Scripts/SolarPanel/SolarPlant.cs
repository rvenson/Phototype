﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SolarPlant : MonoBehaviour {

	const float ELETRIC_UPDATE = 0.5f;

	public Transform gridList;

	public float tension = 0;
	public float current = 0;

	public bool isOn = true;
	private PlantUI plantUI;

	void Start(){
		if(GameObject.FindGameObjectWithTag("Canvas") != null){
			plantUI = GameObject.FindGameObjectWithTag ("Canvas").GetComponent<CanvasController> ().plantUI;
		}

		//plantUI.gameObject.SetActive (true);
		StartCoroutine(CalculatePower ());
	}

	private IEnumerator CalculatePower(){
		while (true) {
			current = 0;
			tension = int.MaxValue;

			if (isOn) {
				foreach (Transform grid in gridList.transform) {
					current += grid.GetComponent<SolarGrid> ().current;
                    //tension += grid.GetComponent<SolarGrid> ().tension;
                    tension = Mathf.Min(grid.GetComponent<SolarGrid>().tension, tension);
                }
			} else
            {
                current = 0;
                tension = 0;
            }

			//RefreshUI ();

			yield return new WaitForSeconds (ELETRIC_UPDATE);
		}
	}
    /*
	private void RefreshUI(){
		if(plantUI.gameObject.activeSelf){
			plantUI.plantSliderCurrent.value = current;
			plantUI.plantSliderTension.value = tension;
            plantUI.plantTotalCurrent.text = current.ToString("F1") + "A";
            plantUI.plantTotalTension.text = tension.ToString("F1") + "V";

            SolarGrid grid1 = gridList.GetChild (0).GetComponent<SolarGrid> ();
			SolarGrid grid2 = gridList.GetChild (1).GetComponent<SolarGrid> ();
			SolarGrid grid3 = gridList.GetChild (2).GetComponent<SolarGrid> ();

			if (grid1.isOn) {
				plantUI.gridAStatus.text = "Online";
			} else {
				plantUI.gridAStatus.text = "Offline";
			}

            if (grid1.GetPower() < 3000.0f)
            {
                plantUI.gridAStatus.text += "\n(Potência baixa!)";
            }

			if (grid2.isOn) {
				plantUI.gridBStatus.text = "Online";
			} else {
				plantUI.gridBStatus.text = "Offline";
			}

            if (grid2.GetPower() < 3000.0f)
            {
                plantUI.gridBStatus.text += "\n(Potência baixa!)";
            }


            if (grid3.isOn) {
				plantUI.gridCStatus.text = "Online";
			} else {
				plantUI.gridCStatus.text = "Offline";
			}

            if (grid3.GetPower() < 3000.0f)
            {
                plantUI.gridCStatus.text += "\n(Potência baixa!)";
            }

            SolarGrid selectedGridA = gridList.GetChild (0).GetComponent<SolarGrid> ();
			plantUI.gridASliderCurrent.value = selectedGridA.current;
			plantUI.gridASliderTension.value = selectedGridA.tension;
			plantUI.gridATextCurrent.text = selectedGridA.current.ToString("F1") + "A";
			plantUI.gridATextTension.text = selectedGridA.tension.ToString("F1") + "V";

			SolarGrid selectedGridB = gridList.GetChild (1).GetComponent<SolarGrid> ();
			plantUI.gridBSliderCurrent.value = selectedGridB.current;
			plantUI.gridBSliderTension.value = selectedGridB.tension;
			plantUI.gridBTextCurrent.text = selectedGridB.current.ToString("F1") + "A";
			plantUI.gridBTextTension.text = selectedGridB.tension.ToString("F1") + "V";

			SolarGrid selectedGridC = gridList.GetChild (2).GetComponent<SolarGrid> ();
			plantUI.gridCSliderCurrent.value = selectedGridC.current;
			plantUI.gridCSliderTension.value = selectedGridC.tension;
			plantUI.gridCTextCurrent.text = selectedGridC.current.ToString("F1") + "A";
			plantUI.gridCTextTension.text = selectedGridC.tension.ToString("F1") + "V";
		}

	}
    */
}
