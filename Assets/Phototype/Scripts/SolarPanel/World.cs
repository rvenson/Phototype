﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class World : MonoBehaviour {

	public Transform eulerSun;
	public int stageID;
	public float startTime;
	public bool randomizeSun = false;

	public int lat = 40;
	public int lon = 0;

	public static int staticStageID;

	public static World instance = null;

	void Awake(){

		instance = this;

		if(randomizeSun){

			int inclination = 90 - Mathf.Abs((int) GameSettings.instance.area25Lat);
			int rotation = Mathf.Sign((int) GameSettings.instance.area25Lat) == 1? 270 : 90;

			eulerSun.eulerAngles = new Vector3(inclination, rotation, 0);
		}
	}

	void Start(){

		startTime = Time.time;
		staticStageID = stageID;
	}

}
