﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelInteractable : InteractableObject {

	private CanvasController canvas;
	private SolarPanel panel;

	private bool isActive;

	void Start(){
		if(GameObject.FindGameObjectWithTag("Canvas") != null){
			canvas = GameObject.FindGameObjectWithTag ("Canvas").GetComponent<CanvasController> ();
		}

        panel = gameObject.GetComponentInParent<SolarPanel>();

    }

	public override void triggerAction ()
	{
		canvas.OpenMainPanelMenu(panel, true);
	}

    public override string TipText()
    {
        return LocalizationManager.instance.GetLocalizedValue("label.tension") + ": " + panel.tension.ToString("F2")
            + " / " + LocalizationManager.instance.GetLocalizedValue("label.current") + ": " + panel.current.ToString("F2")
            + " / " + LocalizationManager.instance.GetLocalizedValue("label.cleaning") + ": " + panel.cleaning.ToString("0%")
            + " / " + LocalizationManager.instance.GetLocalizedValue("label.incidence") + ": " + panel.projectedIncidence.ToString("0%")
            + "%\n" 
            + LocalizationManager.instance.GetLocalizedValue("text.tip.objectInteraction");
    }
}
