﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireController : MonoBehaviour {

	private float life = 4.5f;

	public void Clean(float damage){
		life -= damage;
		if(life < 0){
			this.gameObject.SetActive (false);
		}
	}
}
