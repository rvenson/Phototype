﻿namespace PhototypeAnalytics
{
    [System.Serializable]
    public class Event
    {
        public string key;
        public string value;
        public long time;

        public Event(string key, string value, long time)
        {
            this.key = key;
            this.value = value;
            this.time = time;
        }
    }
}