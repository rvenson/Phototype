﻿using System.Collections.Generic;

namespace PhototypeAnalytics
{
    [System.Serializable]
    public class AnalyticsData {
        public List<Event> events = new List<Event>();

        public void RegistryEvent(string key, string value)
        {
            events.Add(new Event(key, value, System.DateTime.Now.Ticks));
        }
    }
}