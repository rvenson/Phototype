﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PhototypeAnalytics
{
    public class AnalyticsManager : MonoBehaviour
    {

        public static AnalyticsManager instance;
        private AnalyticsData data;

        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }
            else
            {
                Destroy(this);
            }
        }

        private void Start()
        {
            data = new AnalyticsData();
            data.RegistryEvent("Novo", "teste");
            string json = JsonUtility.ToJson(data);
            Debug.Log(json);
        }

    }
}
