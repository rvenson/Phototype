﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossController : MonoBehaviour {

	private int life = 90;

	private Animator anim;
	private Rigidbody rg;
	public GameObject magic;
	public GameObject magicBall;
	public Transform position;
	public AudioClip deathSound;
	private GameObject player;
	public Renderer skin;
	public AudioSource voice;
	public AudioSource growl;

	public StageFinalController stage;

	public GameObject teleportSmoke;

	private AudioSource audioSource;

	public bool isAlive = true;
	private Coroutine attackRoutine;

	void Start(){
		audioSource = gameObject.GetComponent<AudioSource> ();
		anim = gameObject.GetComponent<Animator> ();
		rg = gameObject.GetComponent<Rigidbody> ();
		player = GameObject.FindGameObjectWithTag ("Player");

		attackRoutine = StartCoroutine (Attack());

		AudioManager.instance.RegisterSpatialSource(growl);
		AudioManager.instance.RegisterSpatialSource(voice);
		AudioManager.instance.RegisterSpatialSource(audioSource);

		growl.Play ();
		voice.Play ();
	}

	void Update () {
		if(Input.GetKeyDown(KeyCode.R)){
			Debug.Log ("Attack");
			StartCoroutine (MagicAttack());
		}
		if(Input.GetKeyDown(KeyCode.T)){
			Debug.Log ("TailAttack");
			anim.SetTrigger ("TailAttack");
			rg.AddForce (new Vector3(100, 0, 0));
		}

		if(Input.GetKeyDown(KeyCode.H)){
			StartCoroutine (Teleport ());
		}
	}

	void FixedUpdate(){
		this.transform.LookAt (player.transform);
	}

	public void Hit(){
		anim.SetTrigger ("Damage");
		growl.Play ();
		life -= 90;
		if(life <= 0){
			voice.Stop ();
			isAlive = false;
			StopCoroutine (attackRoutine);
			StartCoroutine (Teleport());
			stage.FinalSequence();
		}
	}

	private IEnumerator Attack(){
		yield return new WaitForSeconds (10f);

		while(true){
			float randTime = Random.Range (7f, 17f);
			StartCoroutine (MagicAttack());
			yield return new WaitForSeconds (randTime);
		}
	}

	private IEnumerator MagicAttack(){
		anim.SetTrigger ("Attack");

		float rand = Random.Range(0.1f, 1f);

		StartCoroutine (scaleBall(rand, 1f));

		magic.SetActive (true);
		yield return new WaitForSeconds (1.3f);
		magic.SetActive (false);
		ShootBall (rand);

		yield return null;
	}

	private IEnumerator scaleBall(float rand, float totalTime){

		float time = 0;

		while(time < totalTime){
			magic.transform.localScale = Vector3.Lerp(
				new Vector3(0, 0, 0), 
				new Vector3(rand, rand, rand),
				time / totalTime);
			time += Time.deltaTime;
			yield return null;
		}

		yield return null;
	}

	private void ShootBall(float rand){
		GameObject ball = Instantiate (magicBall, position.position, new Quaternion());
		ball.transform.localScale = new Vector3 (rand*20,rand*20,rand*20);
		Rigidbody magicRg = ball.GetComponent<Rigidbody> ();

		float distance = Vector3.Distance (player.transform.position, ball.transform.position);
		Debug.Log ("Distance" + distance);

		ball.transform.LookAt(player.transform);
		magicRg.AddForce (ball.transform.forward * 100 * distance * 100);
	}

	private IEnumerator Teleport(){
		skin.material.shader = Shader.Find("Particles/Additive");
		//anim.SetBool ("dead", true);
		audioSource.clip = deathSound;
		audioSource.Play();
		teleportSmoke.SetActive (true);
		teleportSmoke.GetComponent<ParticleSystem>().Play ();
		yield return new WaitForSeconds (3);
		gameObject.transform.localScale = new Vector3 (0, 0, 0);
		yield return new WaitForSeconds (10);
		Destroy (gameObject);
	}
}
